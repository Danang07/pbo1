package segitiga;

public class hello2 {

	public static void main(String[] args) {
	
		//Luas segitiga siku-siku
		float alas, tinggi;
		alas = 6;
		tinggi = 8;
		System.out.println("Rumus luas segitiga siku-siku adalah 1/2 x alas x tinggi");
		System.out.println("Diketahui alas suatu segitiga 6 dan tingginya 8, maka luas segitiga siku-siku = 1/2 x " + alas + " x " + tinggi + " = " + 0.5*alas*tinggi);
		System.out.println();
	}

}
